/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Booking;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Peter"s Windows
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingService implements Serializable {
    
    private static BookingService instance;
    
    public BookingService(){
        Booking booking1 = new Booking("18388167","boo","2018-10-24",null,"2018-10-24","17:46:26","2019-01-02","2019-01-07",5,"No","Gobet","Laurent","lgobet.164785@guest.booking.com",null,"ch","Unknown",2,2,"21303","Ágy 6 személyes koedukált hálóteremben",92,null,10,"No",null);
        Booking booking2 = new Booking("1838877","boo","2018-10-24",null,"2018-10-24","17:46:26","2019-01-02","2019-01-07",5,"No","Gobet","Laurent","lgobet.164785@guest.booking.com",null,"ch","Unknown",2,2,"21303","Ágy 6 személyes koedukált hálóteremben",92,null,10,"No",null);
        Booking booking3 = new Booking("1816877","boo","2018-10-24",null,"2018-10-24","17:46:26","2019-01-02","2019-01-07",5,"No","Gobet","Laurent","lgobet.164785@guest.booking.com",null,"ch","Unknown",2,2,"21303","Ágy 6 személyes koedukált hálóteremben",92,null,10,"No",null);
    
        bookings.add(booking1);
        bookings.add(booking2);
        bookings.add(booking3);
    }
    
    @XmlElement
    private List<Booking> bookings = new ArrayList<>();

    public List<Booking> getBookings() {
        return bookings;
    }
    
    
    
    public static BookingService getInstance(){
        if (instance == null) {
            instance = new BookingService();
        }
        return instance;
    }
    
}
