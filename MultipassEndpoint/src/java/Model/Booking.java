/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Peter's Windows
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Booking implements Serializable{
    @XmlElement
    String Booking_ID;
    @XmlElement
    String Channel;
    @XmlElement
    String Booking_Date;
    @XmlElement
    String Booking_Time;
    @XmlElement
    String Modification_Date;
    @XmlElement
    String Modification_Time;
    @XmlElement
    String Arrival_Date;
    @XmlElement
    String Departure_Date;
    @XmlElement
    int Nights;
    @XmlElement
    String Cacellation;
    @XmlElement
    String Customer_Last_Name;
    @XmlElement
    String Customer_First_Name;
    @XmlElement
    String Customer_Email_Address;
    @XmlElement
    String Customer_Nationatlity;
    @XmlElement
    String Customer_Country;
    @XmlElement
    String Customer_Marketing_OptIn;
    @XmlElement
    int Number_of_roomsbeds;
    @XmlElement
    int Number_of_guests;
    @XmlElement
    String Room_IDs;
    @XmlElement
    String Room_Names;
    @XmlElement
    double Total_Price;
    @XmlElement
    String balace;
    @XmlElement
    int Deposit;
    @XmlElement
    String Marked_as_Read;
    @XmlElement
    String Notes;
    
    public Booking(String Booking_ID, String Channel, String Booking_Date, String Booking_Time, String Modification_Date, String Modification_Time, String Arrival_Date, String Departure_Date, int Nights, String Cacellation, String Customer_Last_Name, String Customer_First_Name, String Customer_Email_Address, String Customer_Nationatlity, String Customer_Country, String Customer_Marketing_OptIn, int Number_of_roomsbeds, int Number_of_guests, String Room_IDs, String Room_Names, int Total_Price, String balace, int Deposit, String Marked_as_Read, String Notes) {
        this.Booking_ID = Booking_ID;
        this.Channel = Channel;
        this.Booking_Date = Booking_Date;
        this.Booking_Time = Booking_Time;
        this.Modification_Date = Modification_Date;
        this.Modification_Time = Modification_Time;
        this.Arrival_Date = Arrival_Date;
        this.Departure_Date = Departure_Date;
        this.Nights = Nights;
        this.Cacellation = Cacellation;
        this.Customer_Last_Name = Customer_Last_Name;
        this.Customer_First_Name = Customer_First_Name;
        this.Customer_Email_Address = Customer_Email_Address;
        this.Customer_Nationatlity = Customer_Nationatlity;
        this.Customer_Country = Customer_Country;
        this.Customer_Marketing_OptIn = Customer_Marketing_OptIn;
        this.Number_of_roomsbeds = Number_of_roomsbeds;
        this.Number_of_guests = Number_of_guests;
        this.Room_IDs = Room_IDs;
        this.Room_Names = Room_Names;
        this.Total_Price = Total_Price;
        this.balace = balace;
        this.Deposit = Deposit;
        this.Marked_as_Read = Marked_as_Read;
        this.Notes = Notes;
    }

    public String getBooking_ID() {
        return Booking_ID;
    }

    public String getChannel() {
        return Channel;
    }

    public String getBooking_Date() {
        return Booking_Date;
    }

    public String getBooking_Time() {
        return Booking_Time;
    }

    public String getModification_Date() {
        return Modification_Date;
    }

    public String getModification_Time() {
        return Modification_Time;
    }

    public String getArrival_Date() {
        return Arrival_Date;
    }

    public String getDeparture_Date() {
        return Departure_Date;
    }

    public int getNights() {
        return Nights;
    }

    public String getCacellation() {
        return Cacellation;
    }

    public String getCustomer_Last_Name() {
        return Customer_Last_Name;
    }

    public String getCustomer_First_Name() {
        return Customer_First_Name;
    }

    public String getCustomer_Email_Address() {
        return Customer_Email_Address;
    }

    public String getCustomer_Nationatlity() {
        return Customer_Nationatlity;
    }

    public String getCustomer_Country() {
        return Customer_Country;
    }

    public String getCustomer_Marketing_OptIn() {
        return Customer_Marketing_OptIn;
    }

    public int getNumber_of_roomsbeds() {
        return Number_of_roomsbeds;
    }

    public int getNumber_of_guests() {
        return Number_of_guests;
    }

    public String getRoom_IDs() {
        return Room_IDs;
    }

    public String getRoom_Names() {
        return Room_Names;
    }

    public double getTotal_Price() {
        return Total_Price;
    }

    public String getBalace() {
        return balace;
    }

    public int getDeposit() {
        return Deposit;
    }

    public String getMarked_as_Read() {
        return Marked_as_Read;
    }

    public String getNotes() {
        return Notes;
    }

    public void setBooking_ID(String Booking_ID) {
        this.Booking_ID = Booking_ID;
    }

    public void setChannel(String Channel) {
        this.Channel = Channel;
    }

    public void setBooking_Date(String Booking_Date) {
        this.Booking_Date = Booking_Date;
    }

    public void setBooking_Time(String Booking_Time) {
        this.Booking_Time = Booking_Time;
    }

    public void setModification_Date(String Modification_Date) {
        this.Modification_Date = Modification_Date;
    }

    public void setModification_Time(String Modification_Time) {
        this.Modification_Time = Modification_Time;
    }

    public void setArrival_Date(String Arrival_Date) {
        this.Arrival_Date = Arrival_Date;
    }

    public void setDeparture_Date(String Departure_Date) {
        this.Departure_Date = Departure_Date;
    }

    public void setNights(int Nights) {
        this.Nights = Nights;
    }

    public void setCacellation(String Cacellation) {
        this.Cacellation = Cacellation;
    }

    public void setCustomer_Last_Name(String Customer_Last_Name) {
        this.Customer_Last_Name = Customer_Last_Name;
    }

    public void setCustomer_First_Name(String Customer_First_Name) {
        this.Customer_First_Name = Customer_First_Name;
    }

    public void setCustomer_Email_Address(String Customer_Email_Address) {
        this.Customer_Email_Address = Customer_Email_Address;
    }

    public void setCustomer_Nationatlity(String Customer_Nationatlity) {
        this.Customer_Nationatlity = Customer_Nationatlity;
    }

    public void setCustomer_Country(String Customer_Country) {
        this.Customer_Country = Customer_Country;
    }

    public void setCustomer_Marketing_OptIn(String Customer_Marketing_OptIn) {
        this.Customer_Marketing_OptIn = Customer_Marketing_OptIn;
    }

    public void setNumber_of_roomsbeds(int Number_of_roomsbeds) {
        this.Number_of_roomsbeds = Number_of_roomsbeds;
    }

    public void setNumber_of_guests(int Number_of_guests) {
        this.Number_of_guests = Number_of_guests;
    }

    public void setRoom_IDs(String Room_IDs) {
        this.Room_IDs = Room_IDs;
    }

    public void setRoom_Names(String Room_Names) {
        this.Room_Names = Room_Names;
    }

    public void setTotal_Price(int Total_Price) {
        this.Total_Price = Total_Price;
    }

    public void setBalace(String balace) {
        this.balace = balace;
    }

    public void setDeposit(int Deposit) {
        this.Deposit = Deposit;
    }

    public void setMarked_as_Read(String Marked_as_Read) {
        this.Marked_as_Read = Marked_as_Read;
    }

    public void setNotes(String Notes) {
        this.Notes = Notes;
    }
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Booking other = (Booking) obj;
        if (this.Booking_ID != other.Booking_ID) {
            return false;
        }
        return true;
    }
    
   
    
}
