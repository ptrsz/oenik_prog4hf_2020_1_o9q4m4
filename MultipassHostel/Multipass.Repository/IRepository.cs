﻿using Multipass.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multipass.Repository
{
    public interface IRepository
    {
        List<booking> getAllBookings();
        List<employee> getAllEmployees();
        List<contractor> getAllContractors();

        bool createBooking(booking item);
        bool createContractor(contractor item);
        bool createEmployee(employee item);

        bool deleteBooking(string id);
        bool deleteEmployee(int id);
        bool deleteContractor(int id);

        bool modifyBooking(string id, string channel);

        bool modifyEmployeeName(int id, string name);
        bool modifyEmployeeSalary(int id, int salary);
        bool modifyEmployeePosition(int id, string position);
        bool modifyEmployeeSchool(int id, string school);
        bool modifyEmployeeAge(int id, int age);

        bool modifyContractorNickname(int id, string nickname);
        bool modifyContractorField(int id, string field);
        bool modifyContractorDept(int id, int dept);
        bool modifyContractorTelephone(int id, string telephone);
        bool modifyContractorAge(int id, int age);
        bool modifyContractorEmployeeContact(int id, employee emp);


        booking getBooking(string id);
        employee getEmployee(int id);
        contractor getContractor(int id);
    }
}
