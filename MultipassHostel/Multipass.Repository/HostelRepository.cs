﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multipass.Data;

namespace Multipass.Repository
{
    public class HostelRepository : IRepository
    {
        public HostelRepository()
        {
        }

        public List<booking> getAllBookings()
        {
            List<booking> ret = new List<booking>();
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            ret = db.bookings.ToList();         
            return ret;
        }

        public List<employee> getAllEmployees()
        {
            List<employee> ret = new List<employee>();
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            ret = db.employees.ToList();
            return ret;
        }
        public List<contractor> getAllContractors()
        {
            List<contractor> ret = new List<contractor>();
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            ret = db.contractors.ToList();
            return ret;
        }

        public bool createBooking(booking item)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                db.bookings.Add(item);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
                
            }           
            return true;
        }

        public bool createContractor(contractor item)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                db.contractors.Add(item);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool createEmployee(employee item)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                db.employees.Add(item);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public booking getBooking(string id)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            booking ret;
            
            ret = db.bookings.Single(x => x.Booking_ID.Equals(id)) as booking;
            return ret;
        }

        public employee getEmployee(int id)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            employee ret;

            ret = db.employees.Single(x => x.Id.Equals(id)) as employee;
            return ret;
        }

        public contractor getContractor(int id)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            contractor ret;

            ret = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
            return ret;
        }

        public bool deleteBooking(string id)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                booking toRemove = db.bookings.Single(x => x.Booking_ID.Equals(id)) as booking;
                db.bookings.Remove(toRemove);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool deleteEmployee(int id)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                employee toRemove = db.employees.Single(x => x.Id.Equals(id)) as employee;
                db.employees.Remove(toRemove);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool deleteContractor(int id)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                contractor toRemove = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
                db.contractors.Remove(toRemove);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyBooking(string id, string channel)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                booking toModify = db.bookings.Single(x => x.Booking_ID.Equals(id)) as booking;
                toModify.Channel = channel;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyEmployeeName(int id, string name)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                employee toModify = db.employees.Single(x => x.Id.Equals(id)) as employee;
                toModify.FullName = name;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyEmployeeSalary(int id, int salary)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                employee toModify = db.employees.Single(x => x.Id.Equals(id)) as employee;
                toModify.Salary = salary;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyEmployeePosition(int id, string position)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                employee toModify = db.employees.Single(x => x.Id.Equals(id)) as employee;
                toModify.Position = position;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyEmployeeSchool(int id, string school)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                employee toModify = db.employees.Single(x => x.Id.Equals(id)) as employee;
                toModify.School = school;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyEmployeeAge(int id, int age)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                employee toModify = db.employees.Single(x => x.Id.Equals(id)) as employee;
                toModify.Age = age;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyContractorNickname(int id, string nickname)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                contractor toModify = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
                toModify.NickName = nickname;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyContractorField(int id, string field)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                contractor toModify = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
                toModify.Field = field;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyContractorDept(int id, int dept)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                contractor toModify = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
                toModify.Dept = dept;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyContractorTelephone(int id, string telephone)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                contractor toModify = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
                toModify.Telephone = telephone;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyContractorAge(int id, int age)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                contractor toModify = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
                toModify.Age = age;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool modifyContractorEmployeeContact(int id, employee emp)
        {
            HostelDatabaseEntities db = new HostelDatabaseEntities();
            try
            {
                contractor toModify = db.contractors.Single(x => x.Id.Equals(id)) as contractor;
                toModify.EmployeeContact = emp.Id;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
