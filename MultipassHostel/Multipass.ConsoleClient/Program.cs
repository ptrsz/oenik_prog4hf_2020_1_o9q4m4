﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.ConsoleClient
{
    // NuGet: add Newtonsoft.JSON
    // Set startup projects: multiple
    // TODO: NSwag / SwashBuckle
    public class Employee
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public Nullable<int> Salary { get; set; }
        public string Position { get; set; }
        public string School { get; set; }
        public Nullable<int> Age { get; set; }

        public override string ToString()
        {
            return $"ID={Id}\tName={FullName}\tSalary={Salary}\tPosition={Position}\tPosition={Position}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:51380/Api/EmployeeApi/";
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            // No error handling at all...
            // using (WebClient client = new WebClient())
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Employee>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                // NameValueCollection postData; // WebClient
                // byte[] responseBytes; // WebClient
                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Employee.FullName), "Tesz Console");
                postData.Add(nameof(Employee.Salary), "9999");
                postData.Add(nameof(Employee.Position), "nonePos");
                // responseBytes = client.UploadValues(url + "add", "POST", postData);
                // response = Encoding.UTF8.GetString(responseBytes);

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int empId = JsonConvert.DeserializeObject<List<Employee>>(json).Single(x => x.FullName == "Tesz Console").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Employee.Id), empId.ToString());
                postData.Add(nameof(Employee.FullName), "Tesz Console");
                postData.Add(nameof(Employee.Salary), "9999");
                postData.Add(nameof(Employee.Position), "nonePos");

                response = client.PostAsync(url + "mod",
                    new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();

                Console.WriteLine("DEL: " + client.GetStringAsync(url + "del/" + empId).Result);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();
                // TODO: Client side filter vs serverside filter, no IQueryable
                // TODO: Client side paging OR server side paging?????
            }
        }
    }
}
