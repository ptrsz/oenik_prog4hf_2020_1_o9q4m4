﻿using Multipass.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Multipass.Logic
{
    public interface ILogic
    {
        // Bookings CRUD
        List<string> getAllBookingsString();
        List<booking> getAllBookings();
        string getBookingString(string id);
        booking getBooking(string id);
        bool createBooking(
            string Booking_ID,
            string Channel,
            DateTime Booking_Date,
            string Booking_Time,
            DateTime Modification_Date,
            string Modification_Time,
            DateTime Arrival_Date,
            DateTime Departure_Date,
            int Nights,
            string Cancellation,
            string Customer_Last_Name,
            string Customer_First_Name,
            string Customer_Email_Address,
            string Customer_Nationality,
            string Customer_Country,
            string Customer_Marketing_OptIn,
            int Number_of_roomsbeds,
            int Number_of_guests,
            string Room_IDs,
            string Room_Names,
            decimal Total_Price,
            string Balance,
            decimal Deposit,
            string Marked_as_Read,
            string Notes
            );
        bool modifyBooking(string id, string channel);
        bool deleteBooking(string id);

        // Employees CRUD
        List<string> getAllEmployeesString();
        List<employee> getAllEmployees();
        string getEmployeeString(int id);
        employee getEmployee(int id);
        bool createEmployee(
            int Id,
            string FullName,
            int Salary,
            string Position,
            string School,
            int Age
            );
        bool modifyEmployeeName(int id, string name);
        bool modifyEmployeeSalary(int id, int salary);
        bool modifyEmployeePosition(int id, string position);
        bool modifyEmployeeSchool(int id, string school);
        bool modifyEmployeeAge(int id, int age);

        bool deleteEmployee(int id);

        // Contractors CRUD
        List<string> getAllContractorsString();
        List<contractor> getAllContractors();
        string getContractorString(int id);
        contractor getContractor(int id);
        bool createContractor(
            int Id,
            string NickName,
            string Field,
            int Dept,
            string Telephone,
            int Age,
            int EmployeeContact
            );
        bool modifyContractorNickname(int id, string nickname);
        bool modifyContractorField(int id, string field);
        bool modifyContractorDept(int id, int dept);
        bool modifyContractorTelephone(int id, string telephone);
        bool modifyContractorAge(int id, int age);
        bool modifyContractorEmployeeContact(int id, employee emp);
        bool deleteContractor(int id);


        // Bookings EXTRA
        List<booking> getBookingsFromSource(string source);
        decimal getAverageStayNights();
        decimal getAveragePay();

        // Employees EXTRA
        int getAverageEmployeeSalary();
        List<string> getEmployeesInDepartment(string department);
        int getCostsOfDepartment(string department);

        // Contractors-Employees EXTRA
        List<string> getEmployeeContractorConnection();
        string employeeWithMaxContractor();
        List<string> getContractorsOfEmployee(string emp);

        // JAVA
        XDocument getBookingsFromJavaServlet(string url);
    }
}
