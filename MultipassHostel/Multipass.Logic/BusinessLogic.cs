﻿using Multipass.Data;
using Multipass.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Multipass.Logic
{
    /// <summary>
    /// Class for the businesslogic
    /// </summary>
    public class BusinessLogic : ILogic
    {
        IRepository repo;
        public BusinessLogic()
        {
            this.repo = new HostelRepository();
        }
        public BusinessLogic(IRepository repo)
        {
            this.repo = repo;
        }
        /// <summary>
        /// getter for all the bookings in the database 
        /// </summary>
        /// <returns>string</returns>
        public List<string> getAllBookingsString()
        {
            List<string> ret = new List<string>();
            List<booking> bookingList = repo.getAllBookings();

            foreach (var item in bookingList)
            {
                String buffer = "";
                PropertyInfo[] props = typeof(booking).GetProperties();
                foreach (var p in props)
                {
                    string propertyName = p.ToString();
                    string value;
                    if (p.GetValue(item) != null)
                    {
                        value = p.GetValue(item).ToString();
                    }
                    else
                    {
                        value = "null";
                    }
                    buffer = buffer + propertyName + "=>" + value + ";";
                }
                ret.Add(buffer);
            }
            return ret;
        }
        /// <summary>
        /// getter for all the bookings in the database
        /// </summary>
        /// <returns>List <booking> </returns>
        public List<booking> getAllBookings()
        {
            return repo.getAllBookings();
        }
        /// <summary>
        /// getter for all the contractors in the database
        /// </summary>
        /// <returns>string</returns>
        public List<string> getAllContractorsString()
        {
            List<string> ret = new List<string>();
            List<contractor> contractorList = repo.getAllContractors();
            // todo convert the bookingList into the ret            
            foreach (var item in contractorList)
            {
                String buffer = "";
                PropertyInfo[] props = typeof(contractor).GetProperties();
                foreach (var p in props)
                {
                    string propertyName = p.ToString();
                    string value;
                    if (p.GetValue(item) != null)
                    {
                        value = p.GetValue(item).ToString();
                    }
                    else
                    {
                        value = "null";
                    }
                    buffer = buffer + propertyName + "=>" + value + ";";
                }
                ret.Add(buffer);
            }
            return ret;
        }
        /// <summary>
        /// getter for all the contractors in the database
        /// </summary>
        /// <returns>List <contractor> </returns>
        public List<contractor> getAllContractors()
        {
            return repo.getAllContractors();
        }
        /// <summary>
        /// getter for all the employees in the database
        /// </summary>
        /// <returns>string</returns>
        public List<string> getAllEmployeesString()
        {
            List<string> ret = new List<string>();
            List<employee> employeeList = repo.getAllEmployees();
            // todo convert the bookingList into the ret            
            foreach (var item in employeeList)
            {
                String buffer = "";
                PropertyInfo[] props = typeof(employee).GetProperties();
                foreach (var p in props)
                {
                    string propertyName = p.ToString();
                    string value;
                    if (p.GetValue(item) != null)
                    {
                        value = p.GetValue(item).ToString();
                    }
                    else
                    {
                        value = "null";
                    }
                    buffer = buffer + propertyName + "=>" + value + ";";
                }
                ret.Add(buffer);
            }
            return ret;
        }
        /// <summary>
        /// getter for all the employees in the database
        /// </summary>
        /// <returns>List <employee></returns>
        public List<employee> getAllEmployees()
        {
            return repo.getAllEmployees();
        }
        /// <summary>
        /// booking creation method
        /// </summary>
        /// <param name="Booking_ID"></param>
        /// <param name="Channel"></param>
        /// <param name="Booking_Date"></param>
        /// <param name="Booking_Time"></param>
        /// <param name="Modification_Date"></param>
        /// <param name="Modification_Time"></param>
        /// <param name="Arrival_Date"></param>
        /// <param name="Departure_Date"></param>
        /// <param name="Nights"></param>
        /// <param name="Cancellation"></param>
        /// <param name="Customer_Last_Name"></param>
        /// <param name="Customer_First_Name"></param>
        /// <param name="Customer_Email_Address"></param>
        /// <param name="Customer_Nationality"></param>
        /// <param name="Customer_Country"></param>
        /// <param name="Customer_Marketing_OptIn"></param>
        /// <param name="Number_of_roomsbeds"></param>
        /// <param name="Number_of_guests"></param>
        /// <param name="Room_IDs"></param>
        /// <param name="Room_Names"></param>
        /// <param name="Total_Price"></param>
        /// <param name="Balance"></param>
        /// <param name="Deposit"></param>
        /// <param name="Marked_as_Read"></param>
        /// <param name="Notes"></param>
        /// <returns>bool, depending on success </returns>
        public bool createBooking(string Booking_ID, string Channel, DateTime Booking_Date, string Booking_Time, DateTime Modification_Date, string Modification_Time, DateTime Arrival_Date, DateTime Departure_Date, int Nights, string Cancellation, string Customer_Last_Name, string Customer_First_Name, string Customer_Email_Address, string Customer_Nationality, string Customer_Country, string Customer_Marketing_OptIn, int Number_of_roomsbeds, int Number_of_guests, string Room_IDs, string Room_Names, decimal Total_Price, string Balance, decimal Deposit, string Marked_as_Read, string Notes)
        {
            booking item = new booking(Booking_ID, Channel, Booking_Date, Booking_Time, Modification_Date, Modification_Time, Arrival_Date, Departure_Date, Nights, Cancellation, Customer_Last_Name, Customer_First_Name, Customer_Email_Address, Customer_Nationality, Customer_Country, Customer_Marketing_OptIn, Number_of_roomsbeds, Number_of_guests, Room_IDs, Room_Names, Total_Price, Balance, Deposit, Marked_as_Read, Notes);
            bool success = repo.createBooking(item);
            return success;
            
        }
        /// <summary>
        /// contractor creation method
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="NickName"></param>
        /// <param name="Field"></param>
        /// <param name="Dept"></param>
        /// <param name="Telephone"></param>
        /// <param name="Age"></param>
        /// <param name="EmployeeContact"></param>
        /// <returns>bool, depending on success </returns>
        public bool createContractor(int Id, string NickName, string Field, int Dept, string Telephone, int Age, int EmployeeContact)
        {
            contractor item = new contractor(Id, NickName, Field, Dept, Telephone, Age, EmployeeContact);
            bool success = repo.createContractor(item);
            return success;
        }
        /// <summary>
        /// employee creation method
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="FullName"></param>
        /// <param name="Salary"></param>
        /// <param name="Position"></param>
        /// <param name="School"></param>
        /// <param name="Age"></param>
        /// <returns>bool, depending on success </returns>
        public bool createEmployee(int Id, string FullName, int Salary, string Position, string School, int Age)
        {
            employee item = new employee(Id, FullName, Salary, Position, School, Age);
            bool success = repo.createEmployee(item);
            return success;
        }
        /// <summary>
        /// booking deletion method
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool, depending on success </returns>
        public bool deleteBooking(string id)
        {
            bool success = repo.deleteBooking(id);
            return success;
        }
        /// <summary>
        /// contractor deletion method
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool, depending on success </returns>
        public bool deleteContractor(int id)
        {
            bool success = repo.deleteContractor(id);
            return success;
        }
        /// <summary>
        /// employee deletion method
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool, depending on success </returns>
        public bool deleteEmployee(int id)
        {
            bool success = repo.deleteEmployee(id);
            return success;
        }
        /// <summary>
        /// booking modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="channel"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyBooking(string id, string channel)
        {
            bool success = repo.modifyBooking(id, channel);
            return success;
        }
        /// <summary>
        /// contractor age modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="age"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyContractorAge(int id, int age)
        {
            bool success = repo.modifyContractorAge(id, age);
            return success;
        }
        /// <summary>
        /// contractor department modificationmethod
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dept"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyContractorDept(int id, int dept)
        {
            bool success = repo.modifyContractorDept(id, dept);
            return success;
        }
        /// <summary>
        /// contractor employee connection modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="emp"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyContractorEmployeeContact(int id, employee emp)
        {
            bool success = repo.modifyContractorEmployeeContact(id, emp);
            return success;
        }
        /// <summary>
        /// contractor field modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="field"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyContractorField(int id, string field)
        {
            bool success = repo.modifyContractorField(id, field);
            return success;
        }
        /// <summary>
        /// contractor nickname modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nickname"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyContractorNickname(int id, string nickname)
        {
            bool success = repo.modifyContractorNickname(id, nickname);
            return success;
        }
        /// <summary>
        /// contractor telephone modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="telephone"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyContractorTelephone(int id, string telephone)
        {
            bool success = repo.modifyContractorTelephone(id, telephone);
            return success;
        }
        /// <summary>
        /// employee age modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="age"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyEmployeeAge(int id, int age)
        {
            bool success = repo.modifyEmployeeAge(id, age);
            return success;
        }
        /// <summary>
        /// employee name modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyEmployeeName(int id, string name)
        {
            bool success = repo.modifyEmployeeName(id, name);
            return success;
        }
        /// <summary>
        /// employee position modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="position"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyEmployeePosition(int id, string position)
        {
            bool success = repo.modifyEmployeePosition(id, position);
            return success;
        }
        /// <summary>
        /// employee salary modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="salary"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyEmployeeSalary(int id, int salary)
        {
            bool success = repo.modifyEmployeeSalary(id, salary);
            return success;
        }
        /// <summary>
        /// employee school modification method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="school"></param>
        /// <returns>bool, depending on success </returns>
        public bool modifyEmployeeSchool(int id, string school)
        {
            bool success = repo.modifyEmployeeSchool(id, school);
            return success;
        }
        /// <summary>
        /// method for get: one booking
        /// </summary>
        /// <param name="id"></param>
        /// <returnsstring></returns>
        public string getBookingString(string id)
        {
            booking b = repo.getBooking(id);
            String ret = "";
            PropertyInfo[] props = typeof(booking).GetProperties();
            foreach (var p in props)
            {
                string propertyName = p.ToString();
                string value;
                if (p.GetValue(b) != null)
                {
                    value = p.GetValue(b).ToString();
                }
                else
                {
                    value = "null";
                }
                ret = ret + propertyName + "=>" + value + ";";
            }

            return ret;
        }
        /// <summary>
        /// method for get: one booking
        /// </summary>
        /// <param name="id"></param>
        /// <returns>booking</returns>
        public booking getBooking(string id)
        {
            return repo.getBooking(id);
        }
        /// <summary>
        /// getter for booking from provided platform source
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public List<booking> getBookingsFromSource(string source)
        {
            List<booking> all = repo.getAllBookings();
            var fromSource = from x in all
                             where x.Channel == source
                             select x;
            return fromSource.ToList();
        }
        /// <summary>
        /// getter for contractor
        /// </summary>
        /// <param name="id"></param>
        /// <returns>string</returns>
        public string getContractorString(int id)
        {
            contractor c = repo.getContractor(id);
            String ret = "";
            PropertyInfo[] props = typeof(contractor).GetProperties();
            foreach (var p in props)
            {
                string propertyName = p.ToString();
                string value;
                if (p.GetValue(c) != null)
                {
                    value = p.GetValue(c).ToString();
                }
                else
                {
                    value = "null";
                }
                ret = ret + propertyName + "=>" + value + ";";
            }

            return ret;
        }
        /// <summary>
        /// getter for contrator
        /// </summary>
        /// <param name="id"></param>
        /// <returns>contractor</returns>
        public contractor getContractor(int id)
        {
            return repo.getContractor(id);
        }
        /// <summary>
        /// getter for average employee salary
        /// </summary>
        /// <returns>int</returns>
        public int getAverageEmployeeSalary()
        {
            int averageSalary = 0;
            int sum = 0;
            List<employee> employees = repo.getAllEmployees();
            foreach (employee item in employees)
            {
                sum += (int)item.Salary;
            }
            averageSalary = sum / employees.Count;
            return averageSalary;
        }
        /// <summary>
        /// getter for average guest spending
        /// </summary>
        /// <returns></returns>
        public decimal getAveragePay()
        {
            List<booking> bookings = repo.getAllBookings();

            var count = bookings.Select(x => x.Total_Price).Count();            
            var sum = (from x in bookings
                      select x.Total_Price)
                      .Sum();
                        
            return sum / count;
            
        }
        /// <summary>
        /// getter for the average number of guest staying for nights
        /// </summary>
        /// <returns></returns>
        public decimal getAverageStayNights()
        {
            List<booking> bookings = repo.getAllBookings();
            var count = bookings.Count();
            var average = (from x in bookings
                           where x.Cancellation == "No"
                           select x.Nights)
                           .Average();

            return (decimal)average;

                    
        }
        /// <summary>
        /// getter for the overall cost of a provided department 
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public int getCostsOfDepartment(string department)
        {
            List<employee> all = repo.getAllEmployees();
            var sum = (from x in all
                       where x.Position == department
                       select x.Salary)
                      .Sum();
            return (int)sum;
        }
        /// <summary>
        /// getter for employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns>string</returns>
        public string getEmployeeString(int id)
        {
            employee e = repo.getEmployee(id);
            String ret = "";
            PropertyInfo[] props = typeof(employee).GetProperties();
            foreach (var p in props)
            {
                string propertyName = p.ToString();
                string value;
                if (p.GetValue(e) != null)
                {
                    value = p.GetValue(e).ToString();
                }
                else
                {
                    value = "null";
                }
                ret = ret + propertyName + "=>" + value + ";";
            }

            return ret;
        }
        /// <summary>
        /// getter for an employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns>employee</returns>
        public employee getEmployee(int id)
        {
            return repo.getEmployee(id);
        }
        /// <summary>
        /// getter for a provided employee's contractor connections
        /// </summary>
        /// <returns>List <string></returns>
        public List<string> getEmployeeContractorConnection()
        {
            List<contractor> allContractor = repo.getAllContractors();
            List<employee> allEmployees = repo.getAllEmployees();
            List<string> ret = new List<string>();
            if (allContractor != null && allEmployees != null)
            {
                var query = from c in allContractor
                            join e in allEmployees on c.EmployeeContact equals e.Id into contactGroups
                            select contactGroups;
                foreach (var item in query)
                {
                    foreach (var i in item)
                    {
                        List<string> cons = new List<string>();
                        foreach (var c in i.contractors)
                        {
                            cons.Add(c.NickName);
                        }
                        ret.Add($"{i.FullName}=>{string.Join(",", cons)}");
                    }
                }
            }    
            return ret;
        }

        /// <summary>
        /// getter for employees in a department
        /// </summary>
        /// <param name="department"></param>
        /// <returns>List <string> </returns>
        public List<string> getEmployeesInDepartment(string department)
        {
            List<employee> allEmployees = repo.getAllEmployees();
            List<string> ret = new List<string>();
            var query = from x in allEmployees
                        where x.Position == department
                        select x;
            foreach (var item in query)
            {
                ret.Add(item.FullName);
            }
            return ret;
        }
        /// <summary>
        /// getter for the employee of the highest number of contractors
        /// </summary>
        /// <returns>string of employee's full name </returns>
        public string employeeWithMaxContractor()
        {
            List<contractor> allContractor = repo.getAllContractors();
            List<employee> allEmployees = repo.getAllEmployees();
            var query = from c in allContractor
                        join e in allEmployees on c.EmployeeContact equals e.Id into contactGroups
                        select contactGroups;
            int max = 0;
            string employee = "";
            foreach (var item in query)
            {                
                foreach (var i in item)
                {
                    if (i.contractors.Count() > max)
                    {
                        max = i.contractors.Count();
                        employee = i.FullName;
                    }
                }
            }
            return employee;
        }
        /// <summary>
        /// getter for a provided employee's contractors
        /// </summary>
        /// <param name="emp"></param>
        /// <returns>List <string> </returns>
        public List<string> getContractorsOfEmployee(string emp)
        {
            List<contractor> allContractor = repo.getAllContractors();
            List<employee> allEmployees = repo.getAllEmployees();
            List<string> ret = new List<string>();
            var query = from c in allContractor
                        join e in allEmployees on c.EmployeeContact equals e.Id into contactGroups                        
                        select contactGroups;
            foreach (var item in query)
            {
                foreach (var i in item)
                {
                    if (i.FullName == emp)
                    {
                        foreach (var c in i.contractors)
                        {
                            ret.Add(c.NickName);
                        }
                    }
                }
            }
            return ret;
        }      

        /// <summary>
        /// initiate connection from java api to get bookings from there
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public XDocument getBookingsFromJavaServlet(string url)
        {
            XDocument xdoc = new XDocument();

            WebClient wb = new WebClient();

            try
            {
                string buffer = wb.DownloadString(url);
                xdoc = XDocument.Parse(buffer);

                
            }
            catch (WebException w)
            {
                return null;
            }

            return xdoc;
        }
    }
}
