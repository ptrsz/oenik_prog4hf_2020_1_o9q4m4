﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Multipass.Logic
{
    public class MenuHandler
    {
        BusinessLogic logic = new BusinessLogic();
        /// <summary>
        /// method to handle menu selection one
        /// </summary>
        public void MenuOne()
        {
            logic.createBooking("1179433891", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            String requestedData = logic.getBookingString("1179433891");
            String req2 = logic.getEmployeeString(1);
            String req3 = logic.getContractorString(2);
            bool req4 = logic.modifyBooking("1179433891", "hw");
            int averageSalary = logic.getAverageEmployeeSalary();
            decimal averagePay = logic.getAveragePay();
            decimal averageStay = logic.getAverageStayNights();
            int costOfDepartement = logic.getCostsOfDepartment("receptionist");
            List<string> contactGroups = logic.getEmployeeContractorConnection();
            List<string> employeesInDepartment = logic.getEmployeesInDepartment("cleaner");
            string mostContractors = logic.employeeWithMaxContractor();
            List<string> contractorsOfEmployee = logic.getContractorsOfEmployee("Thompson Vivien");

            
            List<string> data = logic.getAllBookingsString();
            
            foreach (var item in data)
            {
                Console.WriteLine(item);
            }
        }
        /// <summary>
        /// test method for menu selection two
        /// </summary>
        public void MenuTwo()
        {
            XDocument javaData = logic.getBookingsFromJavaServlet("http://localhost:8080/MultipassEndpoint/AllBookingsServlet");
            Console.WriteLine(javaData.ToString());
            
        }
    }
}
