﻿using AutoMapper;
using Multipass.Logic;
using Multipass.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Multipass.Web.Controllers
{
    public class EmployeeApiController : ApiController
    {
        ILogic logic;
        IMapper mapper;


        public class ApiResult // Single-point-of-use?
        {
            public bool OperationResult { get; set; }
        }

        public EmployeeApiController()
        {
            logic = new BusinessLogic();
            mapper = MapperFactory.CreateMapper();

        }

        // GET api/EmpApi
        // XML... 
        [ActionName("all")]
        [HttpGet]
        // GET api/EmpApi/all
        public IEnumerable<Models.Employee> GetAll()
        {
            var employees = logic.getAllEmployees();
            return mapper.Map<IList<Data.employee>, List<Models.Employee>>(employees);
        }


        // GET api/EmpApi/del/5
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneEmp(int id)
        {
            return new ApiResult() { OperationResult = logic.deleteEmployee(id) };
        }


        // POST api/EmpApi/add + emp
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneEmp(Employee emp)
        {
            logic.createEmployee(emp.Id, emp.FullName, emp.Salary, emp.Position, emp.School, emp.Age);
            return new ApiResult() { OperationResult = true };
        }

        // POST api/EmpApi/mod  + emp
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneCar(Employee emp)
        {
            return new ApiResult()
            {
                OperationResult = logic.modifyEmployeeName(emp.Id,emp.FullName)
            };
        }
    }
}
