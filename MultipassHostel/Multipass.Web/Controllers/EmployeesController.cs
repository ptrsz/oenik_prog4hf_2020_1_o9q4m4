﻿using AutoMapper;
using Multipass.Logic;
using Multipass.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Multipass.Web.Controllers
{
    public class EmployeesController : Controller
    {
        ILogic logic;
        IMapper mapper;
        EmployeeViewModel vm;

        public EmployeesController()
        {
            logic = new BusinessLogic();

            mapper = MapperFactory.CreateMapper();
            vm = new EmployeeViewModel();
            vm.EditedEmployee = new Employee();

            var employees = logic.getAllEmployees();
            vm.ListAllEmployees = mapper.Map<IList<Data.employee>, List<Models.Employee>>(employees);
        }

        private Employee GetEmployeeModel(int id)
        {
            Multipass.Data.employee emp = logic.getEmployee(id);
            return mapper.Map<Data.employee, Models.Employee>(emp);
        }

        // GET: Employees
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("EmployeeIndex", vm);
        }

        // GET: Employees/Details/5
        public ActionResult Details(int id)
        {
            return View("EmployeesDetails",GetEmployeeModel(id));
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedEmployee = GetEmployeeModel(id);
            return View("EmployeeIndex", vm);
        }

        // POST: Employees/Edit/5
        [HttpPost]
        public ActionResult Edit(Employee emp, string editAction)
        {
            try
            {
                if (ModelState.IsValid && emp != null)
                {
                    TempData["editResult"] = "Edit oks";
                    if (editAction == "AddNew")
                    {
                        logic.createEmployee(emp.Id, emp.FullName, emp.Salary, emp.Position, emp.School, emp.Age);
                    }
                    else
                    {
                        //bool success = logic.modifyEmployee --> ez a metodus meg nincs a logicban, csak egyenkenti parameteresen
                        bool success = false;
                        if (!success) TempData["editResult"] = "Edit failed.";
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewData["editAction"] = "Edit";
                    vm.EditedEmployee = emp;
                    return View("EmployeeIndex", emp);
                }

            }
            catch
            {
                return View();
            }
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int id)
        {
            TempData["editResult"] = "Delete failed.";
            if (logic.deleteEmployee(id)) TempData["editResult"] = "Delete successfull";
            return RedirectToAction(nameof(Index));
        }
        
    }
}
