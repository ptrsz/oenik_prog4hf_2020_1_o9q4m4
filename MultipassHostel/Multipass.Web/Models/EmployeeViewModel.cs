﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Multipass.Web.Models
{
    public class EmployeeViewModel
    {

        public Employee EditedEmployee { get; set; }

        public List<Employee> ListAllEmployees { get; set; }
    }
}