﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Multipass.Web.Models
{
    // Form Model
    public class Employee
    {
        [Display(Name = "Emp ID")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Full Name")]
        [Required]
        public string FullName { get; set; }

        [Display(Name = "Salary")]
        [Required]
        public int Salary { get; set; }

        [Display(Name = "Position")]
        [Required]
        public string Position { get; set; }

        [Display(Name = "School")]
        [Required]
        public string School { get; set; }

        [Display(Name = "Age")]
        [Required]
        public int Age { get; set; }
    }
}