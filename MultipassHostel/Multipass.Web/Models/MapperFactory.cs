﻿using AutoMapper;
using AutoMapper.Configuration.Conventions;
using Multipass.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Multipass.Web.Models
{
    // legacy: Mapper.Initialize at AppDomain startup
    // core: services.AddAutoMapper(assembly[])
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Multipass.Data.employee, Multipass.Web.Models.Employee>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                    ForMember(dest => dest.FullName, map => map.MapFrom(src => src.FullName)).
                    ForMember(dest => dest.Salary, map => map.MapFrom(src => src.Salary)).
                    ForMember(dest => dest.Position, map => map.MapFrom(src => src.Position)).
                    ForMember(dest => dest.School, map => map.MapFrom(src => src.School)).
                    ForMember(dest => dest.Age, map => map.MapFrom(src => src.Age));                    
                // ReverseMap().
                // 
            });
            return config.CreateMapper();
        }
    }
}