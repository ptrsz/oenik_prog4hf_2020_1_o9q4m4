﻿using Moq;
using Multipass.Data;
using Multipass.Repository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multipass.Logic.Tests
{
    public class BusinessLogicTest
    {
        
        /// <summary>
        /// test for booking creation
        /// </summary>
        [Test]
        public void createBookingTest()
        {
            // ARRANGE
            Mock<IRepository> mockRepo = new Mock<IRepository>();
            
            booking b = new booking("1179433891", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            mockRepo.Setup(x => x.createBooking(b)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            logic.createBooking("1179433891", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);

            mockRepo.Verify(x => x.createBooking(It.IsAny<booking>()), Times.Once());
        }
        /// <summary>
        /// test for employee creation
        /// </summary>
        [Test]
        public void createEmployeeTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            mockRepo.Setup(x => x.createEmployee(e)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            logic.createEmployee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);

            mockRepo.Verify(x => x.createEmployee(It.IsAny<employee>()), Times.Once);

        }
        /// <summary>
        /// test for contractor creation
        /// </summary>
        [Test]
        public void createContractorTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            contractor c = new contractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);
            mockRepo.Setup(x => x.createContractor(c)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            logic.createContractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);

            mockRepo.Verify(x => x.createContractor(It.IsAny<contractor>()), Times.Once);
        }
        /// <summary>
        /// test for booking delete
        /// </summary>
        [Test]
        public void deleteBookingTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            booking b = new booking("1179433891", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            mockRepo.Setup(x => x.createBooking(b)).Returns(true);
            mockRepo.Setup(x => x.deleteBooking("1179433891")).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            logic.deleteBooking("1179433891");

            mockRepo.Verify(x => x.deleteBooking(It.IsAny<string>()), Times.Once);

        }
        /// <summary>
        /// test for employee delete
        /// </summary>
        [Test]
        public void deleteEmployeeTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            mockRepo.Setup(x => x.createEmployee(e)).Returns(true);
            mockRepo.Setup(x => x.deleteEmployee(1)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            logic.deleteEmployee(1);

            mockRepo.Verify(x => x.deleteEmployee(It.IsAny<int>()), Times.Once);
        }
        /// <summary>
        /// test for contractor delete
        /// </summary>
        [Test]
        public void deleteContractorTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            contractor c = new contractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);
            mockRepo.Setup(x => x.createContractor(c)).Returns(true);
            mockRepo.Setup(x => x.deleteContractor(1)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            logic.deleteContractor(1);

            mockRepo.Verify(x => x.deleteContractor(It.IsAny<int>()), Times.Once);
        }
        /// <summary>
        /// test for all bookings getter
        /// </summary>
        [Test]
        public void getAllBookingsTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            booking b = new booking("1179433891", "booking.com", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            booking c = new booking("9999", "booking.com", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);

            var bookings = new List<booking>() { b, c };

            mockRepo.Setup(boo => boo.getAllBookings()).Returns(bookings);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            var data = logic.getAllBookings();
            int count = data.Count();

            Assert.AreEqual(count, 2);


        }
        /// <summary>
        /// test for all contractors getter
        /// </summary>
        [Test]
        public void getAllContractorsTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            contractor b = new contractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);
            contractor c = new contractor(1, "Kisjani", "vizvezetek", 0, "36301116564", 56, 1);

            var contractors = new List<contractor>() { b, c };

            mockRepo.Setup(boo => boo.getAllContractors()).Returns(contractors);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            var data = logic.getAllContractors();
            int count = data.Count();

            Assert.AreEqual(count, 2);
        }
        /// <summary>
        /// test for all employees getter
        /// </summary>
        [Test]
        public void getAllEmployeesTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            employee f = new employee(2, "Thompson Alma", 20000, "receptionist", "BME", 23);

            var employees = new List<employee>() { e, f };

            mockRepo.Setup(boo => boo.getAllEmployees()).Returns(employees);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            var data = logic.getAllEmployees();
            int count = data.Count();

            Assert.AreEqual(count, 2);
        }
        /// <summary>
        /// test for get: employee's average salary
        /// </summary>
        [Test]
        public void getAverageEmployeeSalaryTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();


            employee e = new employee(1, "Thompson Vivien", 5000, "receptionist", "BME", 23);
            employee f = new employee(2, "Thompson Alma", 5000, "receptionist", "BME", 23);
            employee g = new employee(3, "Thompson Eszter", 5000, "receptionist", "BME", 23);

            var employees = new List<employee>() { e, f, g };

            mockRepo.Setup(boo => boo.getAllEmployees()).Returns(employees);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            double expectedAverage = 5000;
            
            Assert.AreEqual(logic.getAverageEmployeeSalary(), expectedAverage);
            
        }
        /// <summary>
        /// test for get: average number of nights of people
        /// </summary>
        [Test]
        public void getAverageStayNightsTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            booking b = new booking("1179433891", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            booking c = new booking("9999", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 4, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            booking d = new booking("23", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            booking e = new booking("435", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 4, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);

            var bookings = new List<booking>() { b, c , d , e };

            mockRepo.Setup(boo => boo.getAllBookings()).Returns(bookings);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            int expected = 3;

            Assert.AreEqual(logic.getAverageStayNights(), expected);
        }
        /// <summary>
        /// test for get: booking
        /// </summary>
        [Test]
        public void getBookingTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            booking b = new booking("1179433891", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            booking c = new booking("9999", "boo", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            mockRepo.Setup(x => x.createBooking(b)).Returns(true);
            mockRepo.Setup(x => x.createBooking(c)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            booking data = logic.getBooking("9999");

            mockRepo.Verify(x => x.getBooking(It.IsAny<string>()), Times.Once);
        }
        /// <summary>
        /// test for get: booking, provided a source of reservation platform
        /// </summary>
        [Test]
        public void getBookingFromSourceTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            booking b = new booking("1179433891", "booking.com", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);
            booking c = new booking("9999", "booking.com", DateTime.Parse("2018-10-17"), null, DateTime.Parse("2018-10-17"), "06:49:29", DateTime.Parse("2019-01-03"), DateTime.Parse("2019-01-05"), 2, "No", "Azarova", "Victoria", "vazaro.908119@guest.booking.com", null, "ru", "Unknown", 1, 1, "21308", "Double", 49.14m, null, 0, "No", null);

            var bookings = new List<booking>() { b, c };

            mockRepo.Setup(boo => boo.getAllBookings()).Returns(bookings);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            var data = logic.getBookingsFromSource("booking.com");

            Assert.That(data, Is.Not.Empty);

        }
        /// <summary>
        /// test for get: contractor
        /// </summary>
        [Test]
        public void getContractorTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            contractor c = new contractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);
            mockRepo.Setup(x => x.createContractor(c)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            contractor data = logic.getContractor(1);

            mockRepo.Verify(x => x.getContractor(It.IsAny<int>()), Times.Once);
        }
        /// <summary>
        /// test for get: cost of a provided department (sum of salaries in that department)
        /// </summary>
        [Test]
        public void getCostsOfDepartmentTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            employee f = new employee(2, "Thompson Alma", 20000, "receptionist", "BME", 23);
            employee g = new employee(2, "Kis Janos", 20000, "receptionist", "BME", 23);
            employee h = new employee(2, "Nagy Endre", 20000, "receptionist", "BME", 23);
            employee i = new employee(2, "Luke Skywalker", 20000, "receptionist", "BME", 23);

            var employees = new List<employee>() { e, f , g, h , i };

            mockRepo.Setup(boo => boo.getAllEmployees()).Returns(employees);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            int expected = 130000;

            Assert.AreEqual(logic.getCostsOfDepartment("receptionist"), expected);

        }
        /// <summary>
        /// test for get: employee
        /// </summary>
        [Test]
        public void getEmployeeTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            mockRepo.Setup(x => x.createEmployee(e)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            employee data = logic.getEmployee(1);

            mockRepo.Verify(x => x.getEmployee(It.IsAny<int>()), Times.Once);
        }
        /// <summary>
        /// test for get: contracotrs in connection with a provided employee
        /// </summary>
        [Test]
        public void getEmployeeContractorConnectionTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            mockRepo.Setup(x => x.createEmployee(e)).Returns(true);
            contractor c = new contractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);
            mockRepo.Setup(x => x.createContractor(c)).Returns(true);

            
            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            List<string> data = logic.getEmployeeContractorConnection();

            Assert.That(data, Is.Empty);
        }
        /// <summary>
        /// test for get: employees in a provided department
        /// </summary>
        [Test]
        public void getEmployeesInDepartmentTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 5000, "receptionist", "BME", 23);
            employee f = new employee(2, "Thompson Alma", 5000, "receptionist", "BME", 23);
            employee g = new employee(3, "Thompson Eszter", 5000, "cleaner", "BME", 23);

            var employees = new List<employee>() { e, f, g};
            List<string> expected = new List<string>() { "Thompson Vivien", "Thompson Alma" };

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);

            mockRepo.Setup(boo => boo.getAllEmployees()).Returns(employees);

            var value = logic.getEmployeesInDepartment("receptionist");

            CollectionAssert.AreEqual(value, expected);
        }
        /// <summary>
        /// test for get: employee with the maximum number of contractors in connection with him/her
        /// </summary>
        [Test]
        public void employeeWithMaxContractorTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            mockRepo.Setup(x => x.createEmployee(e)).Returns(true);
            contractor c = new contractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);
            mockRepo.Setup(x => x.createContractor(c)).Returns(true);


            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            List<string> data = logic.getEmployeeContractorConnection();

            Assert.That(data, Is.Empty);
        }
        /// <summary>
        /// test for get: contractors in connection with a provided employee
        /// </summary>
        [Test]
        public void getContractorsOfEmployeeTest()
        {
            Mock<IRepository> mockRepo = new Mock<IRepository>();

            employee e = new employee(1, "Thompson Vivien", 50000, "receptionist", "BME", 23);
            mockRepo.Setup(x => x.createEmployee(e)).Returns(true);
            contractor c = new contractor(1, "Sanyibacsi", "vizvezetek", 0, "36301116564", 56, 1);
            mockRepo.Setup(x => x.createContractor(c)).Returns(true);

            BusinessLogic logic = new BusinessLogic(mockRepo.Object);
            List<string> data = logic.getEmployeeContractorConnection();

            Assert.That(data, Is.Empty);

        }        
    }
}
