﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MultipassConsol
{
    using System;
    using Multipass.Logic;

    public class Program
    {
        public static void Main(string[] args)
        {
            MenuHandler menu = new MenuHandler();
            Console.WriteLine("Üdvözöllek a Multipass Hostel Foglalási Rendszerében!\n");
            Console.WriteLine("Kérlek válassz az alábbi menüpontok közül:");
            Console.WriteLine("1. Minden foglalás listázása");
            Console.WriteLine("2. Foglalások CRUD");
            Console.WriteLine("3. Dolgozók CRUD");
            Console.WriteLine("4. Karbantartók CRUD");
            Console.WriteLine("5. Dolgozók  ");
            Console.WriteLine("5. A foglalások frissítése a JAVA foglalási rendszerből");
            Console.WriteLine("6. Kapcsolódás az adatbázishoz");
            menu.MenuOne();
            Console.WriteLine("*****************");
            //menu.MenuTwo();
            Console.ReadLine();
        }
    }
}
